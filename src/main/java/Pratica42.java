import utfpr.ct.dainf.if62c.pratica.Elipse;
import utfpr.ct.dainf.if62c.pratica.Circulo;

/**
 *
 * @author Omero Francisco Bertol
 */

public class Pratica42 {

  public static void main(String[] args) {
    Elipse elipse = new Elipse(3.0, 6.0);
    Circulo circulo = new Circulo(3.0);
    
    System.out.println("Area de " + elipse + " = " + elipse.getArea());
    System.out.println("Perímetro de " + elipse + " = " + elipse.getPerimetro());
    
    System.out.println();
    
    System.out.println("Area de " + circulo + " = " + circulo.getArea());
    System.out.println("Perímetro de " + circulo + " = " + circulo.getPerimetro());  
  }
    
}
