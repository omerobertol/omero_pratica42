package utfpr.ct.dainf.if62c.pratica;

/**
 *
 * @author Omero Francisco Bertol
 */

public interface FiguraComEixos extends Figura {
  public double getEixoMenor();
  public double getEixoMaior();
}